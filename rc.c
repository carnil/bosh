/*
Config file and argument parsing related routines.

$Id: rc.c,v 1.50 2009/03/26 13:44:22 alexsisson Exp $

(C) Copyright 2002-2009 Alex Sisson (alexsisson@gmail.com)

This file is part of bosh.

bosh is free software; you can redistribute it and/or modify
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

bosh is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bosh; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "rc.h"
#include "misc.h"

/*
 * bosh_rc_read
 *
 * Reads a configuration, processes the actions, and stores other options into the arg array
 *
 */
int bosh_rc_read(char *name, bosh_t *bosh, stray_t *arg) {

  FILE *f;
  char s[256],*p,*a,*v;
  int line=0;
  stray_t *origarg;
  int isaction;

  origarg = stray_dup(arg);
  stray_clr(arg);
  stray_addstr(arg,stray_get(origarg,0));
  stray_del(origarg,0);

  /* open config */
  f = fopen(name,"r");
  if(!f)
    bosh_fatal_err(1,"file not found '%s'",name);

  while(!feof(f)) {
    if(!fgets(s,sizeof(s),f)) {
      if(ferror(f))
        bosh_fatal_err(1,"[%s:%02d] read error",conf,line);
      break;
    }

    line++;

    /* it's a comment */
    if(*s=='#')
      continue;

    /* lose \n */
    strswp(s,'\n',0);

    /* it's an empty line */
    if(!*s)
      continue;

    /* try and find equals-sign */
    p = strchr(s,'=');
    if(p) {
      *p = 0;
      p++;
      a = strdup(s); /* attribute */
      v = strdup(p); /* value */

      /* handle value on next line */
      if(!*v) {
        if(!fgets(s,sizeof(s),f)) {
          if(ferror(f))
            bosh_fatal_err(1,"[%s:%02d] read error",conf,line);
          break;
        }
        free(v);
        if(*s=='#')
          continue;
        strswp(s,'\n',0);
        v = strdup(s);
      }

      /* handle multi-line value that uses \ */
      while(v[strlen(v)-1]=='\\') {
        v[strlen(v)-1]='\n';
        if(!fgets(s,sizeof(s),f)) {
          if(ferror(f))
            bosh_fatal_err(1,"[%s:%02d] read error",conf,line);
          break;
        }
        line++;
        if(*s=='#')
          continue;
        strswp(s,'\n',0);
        v = realloc(v,strlen(v)+strlen(s)+1);
        strcat(v,s);
      }

    } else {
      /* check for a {{ }} block */
      p = strstr(s,"{{");
      if(p) {
        *p = 0;
        p++;
        a = strdup(s); /* attribute */
        v = strdup("\0");
        while(1) {
          if(!fgets(s,sizeof(s),f)) {
            if(ferror(f))
              bosh_fatal_err(1,"[%s:%02d] read error",conf,line);
            break;
          }
          if(strncmp(s,"}}",2)==0)
            break;
          line++;
          v = realloc(v,strlen(v)+strlen(s)+1);
          strcat(v,s);
        }
      } else {
         /* error */
         bosh_fatal_err(1,"[%s:%02d] = or {{ expected",conf,line);
      }
    }

#ifdef LOG
    //bosh_log("rc_read: a=%s v=%s\n",a,v);
#endif

    /* process */
    isaction = 0;

    if(strlen(a)==1)
      isaction = 1;

    if(strlen(a)>2) {
      if(a[1]=='[') {
        /* swap new style action syntax to old style for processing by action_set */
        p = malloc(strlen(a+1)+strlen(v)+1);
        sprintf(p,"%s%s",a+1,v);
        free(v);
        v = p;
        a[1] = 0;
        isaction = 1;
      }
    }

    if(isaction) {
      /* action */
      switch(bosh_action_set(bosh,a,v)) {
        case -1:
          bosh_fatal_err(1,"[%s:%02d] invalid action '%c'",conf,line,a[0]);
        case -2:
          bosh_fatal_err(1,"[%s:%02d] invalid action destination '%c'",conf,line,v[1]);
        default:
          break;
      }
    } else {
      /* else copy it into our argv so it can be parsed as an argument */
      p = malloc(strlen(a)+strlen(v)+4); /* --(a)=(v)\0 */
      sprintf(p,"--%s=%s",a,v);
      stray_addstr(arg,p);
    }

    free(a);
    free(v);
  }

  fclose(f);

  /* copy original command line arguments into our new arg array */
  stray_addarr(arg,origarg);

  return 0;
}

int rc_write(char *name, bosh_t *bosh) {

/*
  FILE *f;
  char s[256];
  int i;

  strcpy(s,getenv("HOME"));
  strcat(s,"/.bosh/");
  strcat(s,name);

  f = fopen(s,"w");
  if(!f)
    return -1;

  fprintf(f,"command=%s\n",bosh->command);

  if(bosh->refresh)
    fprintf(f,"refresh=%C\n",bosh->refresh+48);

  fputc('\n',f);

  for(i=0;i<10;i++)
    if(bosh->action[i].command)
      fprintf(f,"%C=%s\n",i+48,bosh->action[i].command);

  for(i=10;i<36;i++)
    if(bosh->action[i].command)
      fprintf(f,"%C=%s\n",i+55,bosh->action[i].command);

  if(bosh->action[37])
    fprintf(f,"enter=%s\n",bosh->action[37]);

  fputc('\n',f);

  fclose(f);
*/
  return 0;
}



/* used by bosh_parseargs below */
long int bosh_atoi(const char *nptr) {
  char *e;
  long int r;
  r = strtol(nptr,&e,10);
  if(*e)
    bosh_fatal_err(1,"%s: invalid numerical argument\n",PACKAGE);
  return r;
}

int bosh_parseargs(bosh_t *bosh, stray_t *arg) {
  int i,n;
  int argc = arg->c;
  char **argv = arg->v;

  argv[0] = strdup(PACKAGE);

  struct option opts[] = { {"help",0,0,'h'},
                           {"version",0,0,'v'},
                           {"autorefresh",1,0,'a'},
                           {"command",1,0,'c'},
                           {"common",1,0,'C'},
                           {"cursorsize",1,0,'s'},
                           {"cursormovement",1,0,'m'},
                           {"multilineseperator",1,0,'M'},
                           {"preaction",1,0,'p'},
                           {"refresh",1,0,'r'},
                           {"shell",1,0,'S'},
                           {"header",1,0,'H'},
                           {"footer",1,0,'F'},
                           {"uservars",1,0,'u'},
                           {0,0,0,0} };

#ifdef LOG
  bosh_log("parseargs: argv before processing:\n");
  bosh_log_stray(arg);
#endif

  /* find first non option arg */
  for(i=1;i<argc;i++) {
    if(argv[i][0]!='-') {
      confpath = argv[i];
      argv[i] = strdup("--");
      break;
    }
  }

  if(confpath) {
    conf = strrchr(confpath,'/');
    conf = conf ? conf+1 : confpath;
    conf = strdup(conf);
    bosh_rc_read(confpath,bosh,arg);
    argc = arg->c;
    argv = arg->v;
#ifdef LOG
    bosh_log("parseargs: argv after rc_read:\n");
    bosh_log_stray(arg);
#endif
  }

  while(1) {
    n = getopt_long(argc,argv,"hv",opts,0);
    if(n<0)
      break;
    switch(n) {

      case 'a':
        bosh->autorefresh = bosh_atoi(optarg);
        break;

      case 'c':
#ifdef LOG
        bosh_log("parseargs: command: %s\n",optarg);
#endif
        bosh->command = strdup(optarg);
        break;

      case 'C':
        bosh->common = strdup(optarg);
        break;

      case 's':
        bosh->cursorsize = bosh_atoi(optarg);
        if(bosh->cursormovement==1)
          bosh->cursormovement = bosh->cursorsize;
        break;

      case 'm':
        bosh->cursormovement = bosh_atoi(optarg);
        break;

      case 'M':
        bosh->multilineseperator = *optarg ? strdup(optarg) : NULL;
        break;

      case 'p':
        bosh->preaction = strdup2(optarg,"\n");
        break;

      case 'r':
        bosh->refresh = (*optarg == '1');
        break;

      case 'S':
        bosh_fatal_err(1,"'shell' is deprecated for the time being. This release of bosh only supports bash");

      case 'H':
        bosh->header = bosh_atoi(optarg);
        break;

      case 'F':
        bosh->footer = bosh_atoi(optarg);
        break;

      case 'u':
        boshuservars = bosh_atoi(optarg);
        break;

      case 'h':
        printf("Please refer to the bosh(1) manpage for usage instructions\n");
        exit(0);

      case 'v':
        printf("%s %s",PACKAGE,VERSION);
#ifdef DEBUG
        printf(" (DEBUG)");
#endif
#ifdef LOG
        printf(" (LOG)");
#endif
        printf("\n");
        exit(0);

      case '?':
        exit(1);
    }
  }

#ifdef LOG
  bosh_log("parseargs: argv after processing:\n");
  bosh_log_stray(arg);
#endif

  /* assemble childargv */
  if(childarg)
    stray_free(childarg);
  childarg = stray_dup(arg);
  n = optind;
  while(n--)
    stray_del(childarg,0);

#ifdef LOG
  bosh_log("arg: childargs:\n");
  bosh_log_stray(childarg);
#endif

  return 0;
}



/*
 *   bosh_action_set
 *
 */
int bosh_action_set(bosh_t *bosh, char *a, char *v) {
  int n, i, vo;

#ifdef LOG
  bosh_log("action_set: a=%s v=%s\n",a,v);
#endif

  /* check its a valid action character. */
  if(isalpha(*a))
    n = toupper(*a) - 55; /* 55 = 'A' - 10 */
  else if(isdigit(*a))
    n = *a - 48;
  else
    return -1;

  /* check if already set */
  if(bosh->action[n].command)
    bosh_fatal_err(1,"duplicate action for '%c'",*a);

  /* parse destination */
  bosh->action[n].dest = BOSH_ACTION_OUTPUT_NONE;
  vo = 0;
  if(v[0]=='[') {
    vo++;
    while(1) {
      if(!v[vo]) {
        break;
      }
      if(v[vo]==']') {
        vo++;
        break;
      }
      switch(vo) {
        case 1:
          switch(v[1]) {
            case BOSH_ACTION_OUTPUT_NONE:
            case BOSH_ACTION_OUTPUT_OVERWRITE:
            case BOSH_ACTION_OUTPUT_ADVANCE:
            case BOSH_ACTION_OUTPUT_ENDCURSES:
              bosh->action[n].dest = v[1];
              break;
            default:
              return -2;
          }
          vo++;
          break;

        case 2:
          if(v[vo]==':') {
            vo++;
            i=0;
            while(v[vo+i]!=']')
              i++;
            bosh->action[n].prompt = malloc(i+1);
            i=0;
            while(v[vo]!=']') {
              bosh->action[n].prompt[i] = v[vo];
              vo++;
              i++;
            }
            bosh->action[n].prompt[i] = 0;
          }
          break;
      }
    }
  }
  if(v[vo])
    bosh->action[n].command = strdup(v+vo);

  return 0;
}
