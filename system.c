/*
bosh system stuff (spawning processes etc)

$Id: system.c,v 1.62 2009/03/26 17:07:51 alexsisson Exp $

(C) Copyright 2004-2009 Alex Sisson (alexsisson@gmail.com)

This file is part of bosh.

bosh is free software; you can redistribute it and/or modify
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

bosh is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bosh; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <errno.h>
#include <sys/stat.h>
#include <ctype.h>
#include <ncurses.h>

#include "system.h"
#include "bosh.h"
#include "misc.h"
#include "rc.h"


#ifdef DEBUG
int debug_leavetmp = 1;
#endif
#ifdef LOG
int log_read = 0;
#endif

static char *logprefix = NULL;



/*
 *  bosh_prepare_*
 *
 *  these generate the script, and returns the array for execv
 *  for a particular shell/interpreter.
 *
 */
static stray_t *bosh_prepare_bash(bosh_t *bosh, char *command, FILE *script) {
  int n;
  stray_t *execvarg;

  char *p = NULL;
#ifdef LOG
  p = malloc(32);
  sprintf(p,"%s[bash] ",logprefix );
#endif

//  fprintf(script,"shift\n"); /* skip secret $1 argument */

  bosh_fprintfl(script,p,"BOSHVARFILE=%s\n",BOSHVARFILE);
  bosh_fprintfl(script,p,"BOSHCONF=\"%s\"\n",conf);
  bosh_fprintfl(script,p,"BOSHPID=%d\n",getpid());
  bosh_fprintfl(script,p,"BOSHPPID=%d\n",getppid());
  if(BOSHPARAM)
    bosh_fprintfl(script,p,"BOSHPARAM=\"%s\"\n",BOSHPARAM);
  if(bosh->title)
    bosh_fprintfl(script,p,"BOSHTITLE=\"%s\"\n",bosh->title);
  for(n=0;n<boshuservars;n++)
    if(boshuservar[n])
      bosh_fprintfl(script,p,"BOSHVAR%d=\"%s\"\n",n+1,boshuservar[n]);

  bosh_fprintfl(script,p,"BOSHFUNCTION() {\n");
  if(bosh->common)
    bosh_fprintfl(script,p,"%s\n",bosh->common);
  if(*bosh->tmpfpipe)
    bosh_fprintfl(script,p,"cat %s | %s\n",bosh->tmpfpipe,command);
  else
    bosh_fprintfl(script,p,"%s\n",command);
  bosh_fprintfl(script,p,"}\n");

  bosh_fprintfl(script,p,"BOSHFUNCTION \"$@\"\n");
  bosh_fprintfl(script,p,"BOSHRETURN=$?\n");

  bosh_fprintfl(script,p,"echo \"$BOSHERR\" > $BOSHVARFILE\n");
  bosh_fprintfl(script,p,"echo \"$BOSHTITLE\" >> $BOSHVARFILE\n");
  for(n=0;n<boshuservars;n++)
    bosh_fprintfl(script,p,"echo \"$BOSHVAR%d\" >> $BOSHVARFILE\n",n+1);

  bosh_fprintfl(script,p,"exit $BOSHRETURN\n");

  /* construct execv args */
  execvarg = stray_new(STRAY_EMPTY);
  stray_addstr_d(execvarg,"/bin/bash");
  stray_addstr_d(execvarg,"--");
  stray_addstr_d(execvarg,bosh->tmpfscript);
  stray_addarr_d(execvarg,childarg);
  return execvarg;
}


/*
static char **bosh_prepare_ruby(bosh_t *bosh, char *cmd, FILE *script) {
  int n;
  char **r;

  bosh_fprintfl(script,logprefix,"BOSHVARFILE='%s'\n",BOSHVARFILE);
  bosh_fprintfl(script,logprefix,"BOSHCONF='%s'\n",conf);
  bosh_fprintfl(script,logprefix,"BOSHPID='%d'\n",getpid());
  bosh_fprintfl(script,logprefix,"BOSHPPID='%d'\n",getppid());
  bosh_fprintfl(script,logprefix,"bosherr=''\n");

  if(BOSHPARAM)
    bosh_fprintfl(script,logprefix,"BOSHPARAM='%s'\n",BOSHPARAM);
  if(bosh->title)
    bosh_fprintfl(script,logprefix,"boshtitle='%s'\n",bosh->title);
  for(n=0;n<boshuservars;n++)
    if(boshuservar[n])
      bosh_fprintfl(script,logprefix,"boshvar%d='%s'\n",n+1,boshuservar[n]);

  bosh_fprintfl(script,logprefix,"END { boshvarfile = File.new(BOSHVARFILE,'w'); boshvarfile.puts(bosherr); boshvarfile.puts(boshtitle); }");

  if(cmd)
    return NULL;
//    bosh_fprintfl(script,logprefix,"cat %s | %s\n",bosh->temp,cmd);
  else
    bosh_fprintfl(script,logprefix,"%s\n",bosh->command);

  r = malloc(sizeof(char*)*4);
  r[0] = "/usr/bin/env";
  r[1] = "ruby";
//  r[2] = malloc(1024); sprintf(r[2],"cat %s | ruby %s %s",bosh->script,childargs);
  r[3] = 0;

  return r;
} */



/* * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *  bosh_popen
 *
 *  this is where a lot of the business gets done.
 *  never called directly, from interface, but from
 *  bosh_{open,pipe,action}.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * */


int bosh_popen(bosh_t *bosh, char *cmd, int cmdtype) {

  int fd[2],parent[3],child[3],n,r=-1;
  pid_t pid;

  if(bosh->childpid) {
//    return -1;
    kill(bosh->childpid,15);
#ifdef LOG
    bosh_log("popen: existing child process %d. kill() returned %s\n",bosh->childpid,errstr);
#endif
    usleep(500);
  }

  /* check that std{in,out,err} are 0,1,2 */
  if(0!=STDIN_FILENO || 1!=STDOUT_FILENO || 2!=STDERR_FILENO)
    return -1;

  /* memset our pipes to invalid fd's */
  memset(parent,-1,sizeof(int)*3);
  memset(child, -1,sizeof(int)*3);

  /* create a pipe for each std stream */
  for(n=0;n<=2;n++) {
    if(pipe(fd))
      goto pipecleanup;
    if(n==0) {
      /* stdin */
      parent[n] = fd[1];
      child [n] = fd[0];
    } else {
      /* stdout/err */
      parent[n] = fd[0];
      child [n] = fd[1];
    }
  }

  strcpy(bosh->tmpfscript,"/tmp/boshs_XXXXXX");
  close(mkstemp(bosh->tmpfscript));

#ifdef LOG
  bosh_log("popen: forking...\n");
#endif

  pid = fork();

  /* PARENT */
  if(pid>0) {

#ifdef LOG
    bosh_log("popen: [parent] fork success: child pid \"%d\"\n",pid);
#endif

    /* close child's ends of the pipes */
    for(n=0;n<=2;n++)
      close(child[n]);

    /* see if child has died already due to an invalid command */
    usleep(BOSH_PIPE_USLEEP);
    if(pid==waitpid(pid,&n,WNOHANG))
      if(WIFEXITED(n))
        if(WEXITSTATUS(n)==127)
          goto pipecleanup;

    /* clear the buffer */
    list_reinit(bosh->buf);

    /* save child details */
    bosh->childin  = parent[0];
    bosh->childout = parent[1];
    bosh->childerr = parent[2];
    bosh->childpid = pid;
    bosh->exit = BOSH_EXIT_UNKNOWN;

    if(cmdtype&BOSH_POPEN_WAIT) {
      #ifdef LOG
      bosh_log("popen: [parent] waiting for child pid \"%d\"\n",pid);
      #endif
      waitpid(bosh->childpid,0,0);
      bosh_close(bosh);
    }

    return 0;
  }

  /* CHILD */
  if(pid==0) {

    FILE *f;
    stray_t *a;

    pid = getpid();

#ifdef LOG
    bosh_log_open("a");
    logprefix = malloc(32);
    sprintf(logprefix,"popen: [child:%d]",pid);
#endif

    /* reset signal handlers - not sure we need to do this: i was testing something */
    signal(SIGTERM,SIG_DFL);
    signal(SIGHUP,SIG_DFL);
    //signal(SIGSEGV,SIG_DFL);
    signal(SIGINT,SIG_DFL);
    signal(SIGPIPE,SIG_DFL);

    /* close parent's ends of the pipes, and dup our ends onto our std streams before closing them */
    for(n=0;n<=2;n++) {
      close(parent[n]);
      if(!(cmdtype&BOSH_POPEN_NODUP)) {
        dup2(child[n],n);
        close(child[n]);
      }
    }

    if(cmdtype & BOSH_POPEN_FILTER) {
      /* we're doing a pipe - preserve current buffer to tmp file */
      char *p;
      strcpy(bosh->tmpfpipe,"/tmp/bosht_XXXXXX");
      #ifdef LOG
      bosh_log("%s created tmp pipe file: \"%s\"\n",logprefix,bosh->tmpfpipe);
      #endif
      f = fdopen(mkstemp(bosh->tmpfpipe),"w");
      list_start(bosh->buf,0);
      while(1) {
        p = list_next(bosh->buf);
        if(!p)
          break;
        fprintf(f,"%s\n",p);
      }
      fclose(f);
    } else {
      *bosh->tmpfpipe = 0;
      if(!cmd)
        cmd = bosh->command;
    }

    /* create and prepare tmpscript for the command/action */
    f = fopen(bosh->tmpfscript,"w");
    if(!f)
      return -1;
    #ifdef LOG
    bosh_log("%s created script: \"%s\"\n",logprefix,bosh->tmpfscript);
    #endif
    a = bosh_prepare_bash(bosh,cmd,f);
    fclose(f);
    chmod(bosh->tmpfscript,0700); /* we should probably set the umask first */

    #ifdef LOG
    bosh_log("%s: execv args:\n",logprefix);
    bosh_log_stray(a);
    bosh_log("%s execv()ing...:\n",logprefix);
    #endif

    execv(a->v[0],a->v);

    #ifdef LOG
    bosh_log("%s execv failed!\n",logprefix);
    #endif

    /* exec failed if we get here - drop through to cleanup. */
    /* or not - this pipecleanup business needs sorting */
    _exit(1);
  }

  /* fork() failed */
pipecleanup:
  for(n=0;n<=2;n++) {
    close(parent[n]);
    close(child[n]);
  }

  return r;
}


int bosh_open(bosh_t *bosh) {
  return bosh_popen(bosh,NULL,0);
}

int bosh_pipe(bosh_t *bosh, char *command) {
  return bosh_popen(bosh,command,BOSH_POPEN_FILTER);
}


/* * * * * * * * * * * * * * * * * * * * * * *
 *
 * bosh_action
 *
 * prepare $BOSH and action destination stuff
 * and then run action.
 *
 * UGLY: needs refactoring
 *
 * * * * * * * * * * * * * * * * * * * * * * */

int bosh_action(bosh_t *bosh, int a) {

  char *action=NULL,*seperator=NULL,*p;
  int dest,i,n;

  if(a==BOSH_ACTION_SPECIAL_SHOWCONF) { /* shov conf (^v) */
    dest = BOSH_ACTION_OUTPUT_ADVANCE;
    if(confpath) {
      action = malloc(strlen(confpath)+7);
      if(action)
        sprintf(action,"cat '%s'",confpath);
    }
    if(!action)
      return -1;

  } else { /* normal action */
    if(bosh->cursorsize>1) { /* need to handle multiline $BOSH */
      seperator = bosh->multilineseperator ? bosh->multilineseperator : "";
      for(n=0,i=0;i<bosh->cursorsize;i++) {
        p = list_get(bosh->buf,bosh->offset+bosh->cursor+i);
        n += strlen(p) + strlen(seperator);
      }
      BOSH = malloc(n);
      if(!BOSH)
        return -1;
      *BOSH = 0;
      for(i=0;i<bosh->cursorsize;i++) {
        p = list_get(bosh->buf,bosh->offset+bosh->cursor+i);
        sprintf(BOSH+strlen(BOSH),"%s%s",p,seperator);
      }
    }
    else /* single line $BOSH */
      BOSH = strdup(list_get(bosh->buf,bosh->offset+bosh->cursor));

    if(bosh->preaction)
      action = strdup2(bosh->preaction,bosh->action[a].command);
    else
      action = strdup(bosh->action[a].command);
    dest = bosh->action[a].dest;

    setenv("BOSH",BOSH,1);
    if(BOSHPARAM)
      setenv("BOSHPARAM",BOSHPARAM,1);
  }

  /* take appropriate action depending on dest */
  switch(dest) {
    case BOSH_ACTION_OUTPUT_OVERWRITE:
      #ifdef LOG
      bosh_log("action: overwrite\n");
      #endif
      //list_reinit(bosh->buf);
      bosh->line = bosh->offset = bosh->cursor = 0;
      bosh_popen(bosh,action,BOSH_POPEN_ACTION);
      break;


    case BOSH_ACTION_OUTPUT_ADVANCE:
      #ifdef LOG
      bosh_log("action: advance\n");
      #endif
      p = NULL;
      /* all very hacky: */
      if(bosh->common)
        p = strdup(bosh->common);
      bosh = bosh_t_new();
      bosh->command = strdup(action);
      bosh->common = p;
      bosh_popen(bosh,NULL,BOSH_POPEN_ACTION);
      if(list_items(boshlist)>1) {
        bosh_t_free(list_get(boshlist,1));
        list_del(boshlist,1);    /* this needs to be corrected if we ever get actions spawning bosh configurations */
      }
      list_add(boshlist,bosh);
      boshlistpos = 1;
      break;


    case BOSH_ACTION_OUTPUT_ENDCURSES:
      #ifdef LOG
      bosh_log("action: end curses\n");
      #endif
      endwin();
      bosh_popen(bosh,action,BOSH_POPEN_ACTION|BOSH_POPEN_NODUP|BOSH_POPEN_WAIT);
      if(bosh->refresh)
        bosh_open(bosh);
      bosh_redraw();
      break;


    case BOSH_ACTION_OUTPUT_NONE:
      #ifdef LOG
      bosh_log("action: none\n");
      #endif
      bosh_popen(bosh,action,BOSH_POPEN_ACTION|BOSH_POPEN_WAIT);
      if(bosh->refresh)
        bosh_open(bosh);
      break;

    default:
      break;
  }

  free(action);
  free(BOSH);
  free(BOSHPARAM);
  BOSH = NULL;
  BOSHPARAM = NULL;
  return 0;
}


/* * * * * * * * * * * * * * * * *
 *
 * bosh_read
 *
 * read from child process if data available
 *
 * returns non-zero if a new line is read
 *
 * * * * * * * * * * * * * * * * */

#define FD_ISVALID(fd) (fd>=0)
#define FD_ISVALIDANDSET(fd,fdset) (FD_ISVALID(fd)?FD_ISSET(fd,fdset):0)
#define FD_SETIFVALID(fd,fdset,n) if(FD_ISVALID(fd)) { FD_SET(fd,fdset); n = hof(n,fd); }

int bosh_read(bosh_t *bosh) {
  static unsigned int readbuflen = 1024;
  static unsigned int readbufpos = 0;
  static unsigned char *readbuf = NULL;
  fd_set fdsetr;
  int n,r,loop=1;
  unsigned char c;

  if(!readbuf)
    readbuf = malloc(readbuflen);

  while(loop) {
    struct timeval tv = {0,0};
    loop = 0;
    FD_ZERO(&fdsetr);
    FD_SETIFVALID(bosh->childout,&fdsetr,n);
    FD_SETIFVALID(bosh->childerr,&fdsetr,n);
    if(select(n+1,&fdsetr,0,0,&tv)>0) {
      n = -1;
      if(FD_ISVALIDANDSET(bosh->childout,&fdsetr))
        n = bosh->childout;
      else if(FD_ISVALIDANDSET(bosh->childerr,&fdsetr))
        n = bosh->childerr;
      if(FD_ISVALID(n)) {
        /* a child fd is set for reading */
        if(read(n,&c,1)>0) {
          /* got data */
          #ifdef LOG
          if(log_read)
            bosh_log("read: fd[%d] buf[%d/%d] got %03d %c\n",n,readbufpos,readbuflen,c,isprint(c)?c:' ');
          #endif
          if(c=='\n'||c=='\r') {
            readbuf[readbufpos] = 0;
            if(c=='\r')
              if(bosh->buf->items)
                list_del(bosh->buf,bosh->buf->items-1);
            list_adddup(bosh->buf,readbuf,readbufpos+1);
            if(bosh->width<readbufpos)
              bosh->width = readbufpos;
            #ifdef LOG
            if(log_read) {
              readbuf[readbufpos] = 0;
              bosh_log("read: line: %s\n",readbuf);
            }
            #endif
            readbufpos = 0;
            r = 1;
          } else {
            readbuf[readbufpos] = c;
            readbufpos++;
            if(readbufpos>=readbuflen) {
              readbuflen *= 2;
              readbuf = realloc(readbuf,readbuflen);
            }
          }
          loop = 1;
        } else {
          /* eof or error */
          #ifdef LOG
          bosh_log("read: fd[%d] eof/error\n",n);
          #endif
          if(n==bosh->childout) {
            bosh->childout = -1;
            close(n);
          }
          else if(n==bosh->childerr) {
            bosh->childerr = -1;
            close(n);
          }
          if(bosh->childout==-1 && bosh->childerr==-1)
            bosh_close(bosh);
        }
      }
    }
  }
  return r;
}



int bosh_write(bosh_t *bosh, char c) {
  return write(bosh->childin,&c,1);
}




/* * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * bosh_close
 *
 * cleans up child process and reads user vars
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * */

void bosh_close(bosh_t *bosh) {
  int i;
  FILE *b;

  close(bosh->childin);
  bosh->childin  = -1;

  if(bosh->childpid) {
    /* need to reap child */
#ifdef LOG
    bosh_log("close: reaping child \"%d\"\n",bosh->childpid);
#endif
    waitpid(bosh->childpid,&i,0);
    if(WIFEXITED(i)) {
      bosh->exit = WEXITSTATUS(i);
#ifdef LOG
      bosh_log("close: child returned %d\n",bosh->exit);
#endif
    } else if(WIFSIGNALED(i)) {
      bosh->exit = WTERMSIG(i) * -1;
#ifdef LOG
      bosh_log("close: child terminated by signal %d\n",bosh->exit*-1);
#endif
    }
    bosh->childpid = 0;
  }

  bosh_unlink(bosh->tmpfscript);

  /* get VARs back from child */
  b = fopen(BOSHVARFILE,"r");
  if(b) {
#ifdef LOG
    bosh_log("close: reading var file \"%s\"\n",BOSHVARFILE);
#endif
    BOSHERR = malloc(256);
    fgets(BOSHERR,256,b);
    strswp(BOSHERR,'\n',0);
    if(strlen(BOSHERR)) {
      /* child set BOSHERR */
      fclose(b);
      bosh_finish(bosh->exit);
    }
    free(BOSHERR);
    BOSHERR = NULL;

    /* read title */
    free(bosh->title);
    bosh->title = malloc(256);
    fgets(bosh->title,256,b);
    strswp(bosh->title,'\n',0);
    if(!strlen(bosh->title)) {
      free(bosh->title);
      bosh->title = NULL;
    }
#ifdef LOG
    else
      bosh_log("close:   varfile: bosh->title \"%s\"\n",bosh->title);
#endif

    /* read user vars */
    for(i=0;i<boshuservars;i++) {
      free(boshuservar[i]);
      boshuservar[i] = malloc(256);
      if(!fgets(boshuservar[i],256,b)) {
        free(boshuservar[i]);
        boshuservar[i] = NULL;
        break;
      }
      strswp(boshuservar[i],'\n',0);
#ifdef LOG
      bosh_log("close:   varfile: BOSHVAR%d/uservar[%d] \"%s\"\n",i+1,i,boshuservar[i]);
#endif
      if(!strlen(boshuservar[i])) {
        free(boshuservar[i]);
        boshuservar[i] = NULL;
      }
    }
    fclose(b);

    bosh_unlink(BOSHVARFILE);
    bosh_unlink(bosh->tmpfpipe);
  }
}


/* * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * bosh_unlink
 *
 * unlink wrapper
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * */

int bosh_unlink(const char *path) {
#ifdef DEBUG
  if(debug_leavetmp) {
#ifdef LOG
    bosh_log("unlink: left %s\n",path);
#endif
    return 0;
  } else {
#ifdef LOG
    bosh_log("unlink: %s\n",path);
#endif
    return unlink(path);
  }
#else
#ifdef LOG
  bosh_log("unlink: %s\n",path);
#endif
  return unlink(path);
#endif
}
