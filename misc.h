/*
Misc

$Id: misc.h,v 1.10 2009/03/20 12:33:46 alexsisson Exp $

(C) Copyright 2005-2009 Alex Sisson (alexsisson@gmail.com)

This file is part of bosh.

bosh is free software; you can redistribute it and/or modify
under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

bosh is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with bosh; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


#ifndef MISC_H_INCLUDED
#define MISC_H_INCLUDED

#include <getopt.h>
#include <stdio.h>
#include <string.h>

#include "stray.h"

#define strequ(s1,s2) (!strcmp(s1,s2))

#define lof(a,b) (a<b?a:b) /* lower of */
#define hof(a,b) (a>b?a:b) /* higher of */
#define errstr   (strerror(errno))

char *strdup2(const char *s1, const char *s2);
char *strmak(size_t len, int c);
char *strswp(char *s, int c, int replace);

char *getopt_mkoptstring(struct option *longopts);


void bosh_fatal_err(int code, char *format, ...);
void bosh_fprintfl(FILE *stream, char *logprefix, const char *format, ...);

#ifdef LOG
void bosh_log_open(const char *mode);
void bosh_log(char *format, ...);
void bosh_log_stray(stray_t *a);
#endif

int bosh_regex_init(char *s);
int bosh_regex_try(char *s);

#endif
