/*
bosh system stuff (spawning processes etc)

$Id: system.h,v 1.17 2009/03/26 14:31:01 alexsisson Exp $

(C) Copyright 2004-2009 Alex Sisson (alexsisson@gmail.com)

*/

#ifndef SYSTEM_H_INCLUDED
#define SYSTEM_H_INCLUDED

/* includes */
#include "bosh.h"

#define errstr   (strerror(errno))

int bosh_open(bosh_t *bosh);
int bosh_pipe(bosh_t *bosh, char *command);
int bosh_action(bosh_t *bosh, int a);

int bosh_read(bosh_t *bosh);
int bosh_write(bosh_t *bosh, char c);
void bosh_close(bosh_t *bosh);
int bosh_unlink(const char *path);

#endif
